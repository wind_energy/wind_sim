
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <thread>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>
#include "std_msgs/Float32.h"

//#include <ds_sim/common.hpp>

#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Accel.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/WrenchStamped.h>

#include <boost/bind.hpp>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/JointState.h>
#include <algorithm>

// Functionality from GDAL for projections
#include <gdal/ogr_spatialref.h>

#include <ds_libtrackline/Trackline.h>
#include <ds_control_msgs/GoalLegLatLon.h>
#include <ds_control_msgs/BottomFollow1D.h>
#include <ds_multibeam_msgs/MultibeamRaw.h>
#include <ds_util/angle.h>

#include <lrauv_dynamics.h>

// Must be the same as in wind_hydro_plugin
#define PROP_RPM_FACTOR 10.0

bool loadString(std::string &result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin lrauvdynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<std::string>(tag);

    // remove any double-quotes
    result.erase(std::remove(result.begin(), result.end(), '\"'),result.end());

    ROS_INFO_STREAM("ROS plugin lrauvdynamics load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadDouble(double& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin lrauvdynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<double>(tag);

    ROS_INFO_STREAM("ROS plugin lrauvdynamics load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadInt(int& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin lrauvdynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<int>(tag);

    ROS_INFO_STREAM("ROS plugin lrauvdynamics/servo load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

#define fsat(x,flim) (((x)>(flim))? (flim) : (((x)<(-flim))? (-flim): (x)))

void line_follow(const ds_trackline::Trackline& trackline, const double& lat, const double& lon, const double& heading_deg, double& rudder_deg) {
  
  using namespace ds_trackline;
  
  Trackline::VectorEN along_across = trackline.lonlat_to_trackframe(lon,lat);


  
  double dl = along_across[1];
  double psil = trackline.getCourseRad();
  double psid = psil + fsat(-0.1*dl,60.0*M_PI/180.0);
  double da = ds_util::angular_separation_radians(psid,heading_deg*M_PI/180.0);
  double dr = fsat(1.0*da,25*M_PI/180.0);

  // ROS_INFO_STREAM("Along: " << along_across[0] << "  Across: " << along_across[1] << " psil: " << psil*180.0/M_PI << " psid: " << psid*180.0/M_PI << "  heading: " << heading_deg);
  
  rudder_deg = dr*180.0/M_PI;
  
}

// R600-style bottom following with minimum altitude, maximum and minimum depths.
void bottom_follow(const double& altd, const double& alt, const double& depth, const double& min_depth, const double& max_depth, const double& pitch_deg, double& sternplane_deg ) {
  // determine desired depth
  double depth_bottom = alt + depth;
  double depthd = min_depth; // default is depth min.
  if (depth_bottom > depth + altd ) { depthd = depth_bottom - altd; } // bottom-follow 
  if (depthd > max_depth) { depthd = max_depth; } // impose maximum depth limit

  // compute depth error
  double deptherr = depth - depthd;

  // Set desired pitch proportional to depth error up to saturation.
  double theta = pitch_deg*M_PI/180.0;
  double thetad = fsat( deptherr*3*M_PI/180.0, 25);  // 3 deg pitch per m depth error up to 25 deg.

  sternplane_deg = fsat(0.1*(theta-thetad)*180.0/M_PI,15); // saturate sternplanes at 15 deg absolute as proxy for aoa

  //ROS_INFO_STREAM("sternplane_deg: " << sternplane_deg << "  depth: " << depth << " pitch_deg: " << pitch_deg << " alt: " << alt << " altd:" << altd << "  min_depth: " << min_depth << "max_depth" << max_depth << "deptherr" << deptherr);
  
}


namespace gazebo {
class LrauvDynamics : public ModelPlugin {
  public:
    LrauvDynamics() {}

    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) {

      // Set up coordinate transforms.
      srs_geo.importFromEPSG(4326); // WGS 84
      srs_proj.importFromEPSG(26987);
      poCT_proj2geo = OGRCreateCoordinateTransformation(&srs_proj, &srs_geo);
      
      //
      this->model = _parent;
      this->body_link = _parent->GetLink();
      this->sternplane_joint = _parent->GetJoint("horizontal_servo");
      this->rudder_joint = _parent->GetJoint("vertical_servo");
      this->propeller_joint = _parent->GetJoint("main_propeller_hub");
      
      // Moving links in gazebo requires dynamics.  Setting position or velocity
      // presumably creates infinite accelerations which cause problems.
      this->rudder_pid = common::PID(1.0, 0, 0.0);
      this->model->GetJointController()->SetPositionPID(
          this->rudder_joint->GetScopedName(), this->rudder_pid);        // Apply the P-controller to the joint.
      this->sternplane_pid = common::PID(1.0, 0, 0.0);
      this->model->GetJointController()->SetPositionPID(
          this->sternplane_joint->GetScopedName(), this->sternplane_pid);  // Apply the P-controller to the joint.


      // propeller
      this->propeller_pid = common::PID(0.01, 1.0, 0.0, 10.0, -10.0);
      this->model->GetJointController()->SetVelocityPID(
          this->propeller_joint->GetScopedName(), this->propeller_pid);  // Apply the P-controller to the joint.


      // Trackline
      this->trackline = new ds_trackline::Trackline(0,90,0,90);
      this->trackline_enabled = false;

      // Bottom Follower parameters
      this->bf = ds_control_msgs::BottomFollow1D();

      // Altitude
      this->altitude = 0.0;
      
      // Listen to the update event
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&LrauvDynamics::OnUpdate, this, _1));


      // Initialize ros, if it has not already bee initialized.
      if (!ros::isInitialized())
	{
	  int argc = 0;
	  char **argv = NULL;
	  ros::init(argc, argv, "gazebo_client",
		    ros::init_options::NoSigintHandler);
	}

      // Create our ROS node. This acts in a similar manner to
      // the Gazebo node
      this->rosNode.reset(new ros::NodeHandle("gazebo_client")); // what does this name do?  no new node shows up, instead /gazebo has the subscription below.
      
      // Subscriptions
      ros::SubscribeOptions so1 =
	ros::SubscribeOptions::create<std_msgs::Float32>(
							 "/" + this->model->GetName() + "/rudder_cmd_deg",
							 1,
							 boost::bind(&LrauvDynamics::OnRudderCmdMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subRudderCmd = this->rosNode->subscribe(so1);
      ros::SubscribeOptions so2 =
	ros::SubscribeOptions::create<std_msgs::Float32>(
							 "/" + this->model->GetName() + "/sternplane_cmd_deg",
							 1,
							 boost::bind(&LrauvDynamics::OnSternplaneCmdMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subSternplaneCmd = this->rosNode->subscribe(so2);
      ros::SubscribeOptions so3 =
	ros::SubscribeOptions::create<std_msgs::Float32>(
							 "/" + this->model->GetName() + "/propeller_cmd_rpm",
							 1,
							 boost::bind(&LrauvDynamics::OnPropellerCmdMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subPropellerCmd = this->rosNode->subscribe(so3);
      ros::SubscribeOptions so4 =
	ros::SubscribeOptions::create<ds_control_msgs::GoalLegLatLon>(
							 "/" + this->model->GetName() + "/trackline_goal",
							 1,
							 boost::bind(&LrauvDynamics::OnTracklineGoalMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subTracklineGoal = this->rosNode->subscribe(so4);

      ros::SubscribeOptions so5 =
	ros::SubscribeOptions::create<ds_control_msgs::BottomFollow1D>(
							 "/" + this->model->GetName() + "/bf_cmd",
							 1,
							 boost::bind(&LrauvDynamics::OnBottomFollowMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subBottomFollow = this->rosNode->subscribe(so5);
      
      ros::SubscribeOptions so6 =
       	ros::SubscribeOptions::create<ds_multibeam_msgs::MultibeamRaw>(
								       "/" + this->model->GetName() + "/sensors/multibeam/raw_multibeam",
								       1,
								       boost::bind(&LrauvDynamics::OnMultibeamRawMsg, this, _1),
								       ros::VoidPtr(), &this->rosQueue);
      this->subMultibeamRaw = this->rosNode->subscribe(so6);
      
      
      // Spin up the queue helper thread.
      this->rosQueueThread =
	std::thread(std::bind(&LrauvDynamics::QueueThread, this));
      
      
    }

    void OnUpdate(const common::UpdateInfo& _info) {

      // Apply high-level controllers

      if (this->trackline_enabled)
	{
	  ignition::math::Pose3d pose = this->body_link->WorldPose();
	  double e = pose.Pos().X();
	  double n = pose.Pos().Y();
	  double psi = M_PI/2.0-pose.Rot().Yaw();
	  double dr_deg;
	  
	  // transform vehicle position into lat/lon.
	  double tLon = e;
	  double tLat = n;
	  this->poCT_proj2geo->Transform( 1, &tLon, &tLat);
	  //ROS_INFO_STREAM("Geographic location: " << tLat << " " << tLon );
	  line_follow(*this->trackline, tLat, tLon, psi*180.0/M_PI, dr_deg);
	  SetRudderAngleDeg(dr_deg);
	  
	  //qqqqqqROS_INFO_STREAM("Line follower set rudder to " << dr_deg << " deg.");

	  // Bottom follow
	  double sternplane_deg;
	  bottom_follow(this->bf.commanded_altitude, this->altitude, -pose.Pos().Z(), this->bf.depth_goal, this->bf.depth_floor, -pose.Rot().Pitch()*180.0/M_PI, sternplane_deg);
	  SetSternplaneAngleDeg(sternplane_deg);
	  
	}

    }


    void initState(const common::UpdateInfo& _info) {

      // Is this needed if using gazebo for dynamics instead of just
      // for display?  Shouldn't this be in one of the initialization files?
      //body_link->SetWorldPose(...);
      
    }


  /// \brief Set the sternplane angle
  /// \param[in] _ds_deg sternplane angle in deg
  void SetSternplaneAngleDeg(const double &_ds_deg)
  {
    this->model->GetJointController()
      ->SetPositionTarget(this->sternplane_joint->GetScopedName(), _ds_deg*M_PI/180.0);
  }

  /// \brief Set the rudder angle
  /// \param[in] _dr_deg rudder angle in deg
  void SetRudderAngleDeg(const double &_dr_deg)
  {
    this->model->GetJointController()
      ->SetPositionTarget(this->rudder_joint->GetScopedName(), _dr_deg*M_PI/180.0);
  }

  /// \brief Set the propeller rpm.
  /// \param[in] _prop_rpm propeller speed in rpm
  void SetPropellerRpm(const double &_prop_rpm)
  {
    this->model->GetJointController()
      ->SetVelocityTarget(this->propeller_joint->GetScopedName(), _prop_rpm*2.0*M_PI/60.0/PROP_RPM_FACTOR);
  }

  /// \brief Set the propeller rpm.
  /// \param[in] _prop_rpm propeller speed in rpm
  void SetTracklineGoal(const ds_control_msgs::GoalLegLatLonConstPtr goalll)
  {

    // ds_tracklines are always in lat/lon and always use their own projection, which is always transverse
    // mercator or UPS depending on the lat/lon of the end point.  gazebo is using who-knows-what to
    // compute lat/lon (you give it an origin, but the projection is fixed presumably).
    // so that means when we feed vehicle position into trackline computations, we first have to unproject into
    // lat lon.

    // populate trackline object.
    // @@@@@ this leaks memory! @@@@@@@  need to destroy the previous trackline first.
    this->trackline = new ds_trackline::Trackline(goalll->line_start.longitude,goalll->line_start.latitude,goalll->line_end.longitude,goalll->line_end.latitude);
    this->trackline_enabled = true;

    //ROS_INFO_STREAM("New trackline: " << this->trackline);

  }

  void SetBottomFollow(const ds_control_msgs::BottomFollow1DConstPtr bf_)
  {
   this->bf = *bf_;
  }

  void SetAltitude(const ds_multibeam_msgs::MultibeamRawConstPtr mb_)
  {
    this->altitude = (*std::min_element(mb_->twowayTravelTime.begin(),mb_->twowayTravelTime.end()))/2.0*1500.0; // close enough
  }
  
  protected:

    gazebo::physics::ModelPtr model;
    gazebo::physics::LinkPtr body_link;
    gazebo::physics::JointPtr sternplane_joint;
    gazebo::common::PID sternplane_pid;
    gazebo::physics::JointPtr rudder_joint;
    gazebo::common::PID rudder_pid;
    gazebo::physics::JointPtr propeller_joint;
    gazebo::common::PID propeller_pid;

  gazebo::event::ConnectionPtr updateConnection;

  ds_trackline::Trackline* trackline;
  bool trackline_enabled;

  ds_control_msgs::BottomFollow1D bf;

  double altitude;

  OGRSpatialReference srs_proj; // spatial reference system
  OGRSpatialReference srs_geo; // spatial reference system

  OGRCoordinateTransformation *poCT_proj2geo;

  
private:
    void OnRudderCmdMsg(const std_msgs::Float32ConstPtr &_msg)
    {
      this->SetRudderAngleDeg(_msg->data);
    }
  void OnSternplaneCmdMsg(const std_msgs::Float32ConstPtr &_msg)
    {
      this->SetSternplaneAngleDeg(_msg->data);
    }
    void OnPropellerCmdMsg(const std_msgs::Float32ConstPtr &_msg)
    {
      this->SetPropellerRpm(_msg->data);
    }  
  void OnTracklineGoalMsg(const ds_control_msgs::GoalLegLatLonConstPtr &_msg)
    {
      this->SetTracklineGoal(_msg);
    }  
  void OnBottomFollowMsg(const ds_control_msgs::BottomFollow1DConstPtr &_msg)
    {
      this->SetBottomFollow(_msg);
    }
  void OnMultibeamRawMsg(const ds_multibeam_msgs::MultibeamRawConstPtr &_msg)
    {
      this->SetAltitude(_msg);
    }
  

/// \brief ROS helper function that processes messages
private: void QueueThread()
{
  static const double timeout = 0.01;
  while (this->rosNode->ok())
  {
    this->rosQueue.callAvailable(ros::WallDuration(timeout));
  }
}

  /// \brief A node use for ROS transport
private: std::unique_ptr<ros::NodeHandle> rosNode;

/// \brief A ROS subscriber
private: ros::Subscriber subRudderCmd;
private: ros::Subscriber subSternplaneCmd;
private: ros::Subscriber subPropellerCmd;
private: ros::Subscriber subTracklineGoal;
private: ros::Subscriber subBottomFollow;
private: ros::Subscriber subMultibeamRaw;
  
/// \brief A ROS callbackqueue that helps process messages
private: ros::CallbackQueue rosQueue;

/// \brief A thread the keeps running the rosQueue
private: std::thread rosQueueThread;
  
};

GZ_REGISTER_MODEL_PLUGIN(LrauvDynamics)
};

