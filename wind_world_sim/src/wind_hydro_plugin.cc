#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

// Functionality from GDAL for projections
#include <gdal/ogr_spatialref.h>

// ocean_model_interfaces for currents.
#include <model_interface/ModelInterface.h>
#include <model_interface/ModelData.h>
#include <general_models/ConstantModel.h>
#include <fvcom/FVCOM.h>

// ROS for publishing some status
#include "ros/ros.h"
#include <geometry_msgs/Vector3.h>

// For AWS metrics
#include <ros_monitoring_msgs/MetricList.h>

// For battery
#include <std_msgs/Float64.h>

#include <boost/date_time/posix_time/posix_time_types.hpp>


// Must be the same as in lrauv_dynamics_plugin.cc
#define PROP_RPM_FACTOR 10.0


enum ocean_currents_model_t
  {
    NONE,
    CONSTANT,
    FVCOM
  };


struct ocean_currents_t
{
  // model type
  ocean_currents_model_t model;

  // constant current parameters
  double constant_east;
  double constant_north;
  
  // fvcom parameters
  int fvcom_epsg;
  std::string fvcom_uri;
  OGRSpatialReference fvcom_srs;
  OGRCoordinateTransformation *fvcom_poCT;
  
};


struct ocean_effects_t
{
  ocean_currents_t currents;
};


namespace gazebo
{
  class WindHydroPlugin : public WorldPlugin
  {
  public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
    {

      counter = 0;
      lastSimTime = gazebo::common::Time(0.0);
      initializing = true;
      
      this->world = _parent;
      this->sdf = _sdf;

      // Make sure the ROS node for Gazebo has already been initialized                                                                                    
      if (!ros::isInitialized())
	{
	  ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
			   << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
	  return;
	}
      this->rosnode_ = new ros::NodeHandle("world_hydro_plugin");
      this->pub_ = this->rosnode_->advertise<geometry_msgs::Vector3>("current",0);     // @@@ need a vector of these that publish on different channels for each model.  just hacking this for now.

      this->rpm_pub_ = this->rosnode_->advertise<std_msgs::Float64>("rpm",0);     // @@@ same as above. hacking this for now.
      this->dt_pub_ = this->rosnode_->advertise<std_msgs::Float64>("dt",0);     // @@@ same as above. hacking this for now.
      this->battery_pub_ = this->rosnode_->advertise<std_msgs::Float64>("battery",0);     // @@@ same as above. hacking this for now.
      battery_whrs = 15000; // This is a default but should be readable as config for each vehicle
      simulate_battery = false;

      // This is for AWS metrics publishing for the cloudwatch metrics collector node
      this->metrics_pub_ = this->rosnode_->advertise<ros_monitoring_msgs::MetricList>("/metrics",0);
      
      // Load the spatial reference system (simulation coordinates)
      this->sdf->PrintValues("dfs");
      this->srs.importFromEPSG(this->sdf->GetElement("projection")->Get<int>("epsg"));
    
      // Load the currents model parameters
      sdf::ElementPtr oceanSDF = this->sdf->GetElement("ocean_effects");
      if ( oceanSDF->HasElement("currents") )
	{
	  this->ocean_effects.currents.model = ocean_currents_model_t::NONE;
	  std::string model_type = oceanSDF->GetElement("currents")->Get<std::string>("model");
	  if ( model_type == "fvcom" || model_type == "FVCOM" )
	    {
	      this->ocean_effects.currents.model = ocean_currents_model_t::FVCOM;

	      // Read parameters for model to use.
	      sdf::ElementPtr fvcomSDF = oceanSDF->GetElement("currents")->GetElement("fvcom");	    
	      this->ocean_effects.currents.fvcom_uri = fvcomSDF->Get<std::string>("uri");
	      this->ocean_effects.currents.fvcom_srs.importFromEPSG(fvcomSDF->GetElement("projection")->Get<int>("epsg"));
	    }
	  // @@@ other models and default to constant 0,0.
	}
    
      // Listen to the update event.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&WindHydroPlugin::OnUpdate, this, _1));

    }
  public: void OnUpdate(const common::UpdateInfo& _info)
    {
    
      // Walk though all active models and compare against existing list.
      // Add any new ones, delete any that have disappeared.
      for (auto& ii : this->world->Models())
	{
	  bool isnew =  true;
	  for ( auto& iii : this->models )
	    {
	      if ( ii->GetId() == std::get<0>(iii)->GetId() )
		{
		  isnew = false;
		}
	    }
	  if ( isnew ) // in the list of active models but not in the internal list
	    {

	      this->models.push_back(std::make_tuple(ii,static_cast<ocean_model_interfaces::ModelInterface*>(nullptr))); // @@@ add the other physics models here.

	      // @@@ this is where we traverse the SDF! for <hydrodynamics>

	      // define if the model should include battery consumption. This is probably done better in another plugin eventually.
	      // if (ii->GetSDF()->HasElement("plugin"))
	      // 	{
	      // 	  sdf::ElementPtr pluginSDF = ii->GetSDF()->GetElement("plugin");
	      // 	  while (pluginSDF) 
	      // 	    {
	      // 	      if (pluginSDF->HasElement("battery"))
	      // 		{
	      // 		  sdf::ElementPtr oceanSDF = pluginSDF->GetElement("battery");
	      // 		  if ( oceanSDF->HasElement("simulate") && oceanSDF->Get<bool>("simulate"))
	      // 		    {
	      // 		      simulate_battery = true;
	      // 		    }
	      // 		}
	      // 	    }
	      // 	}
	
	      
	      // don't want to maintain a list of all possible models, because many of them will not care.

	      if (ii->GetSDF()->HasElement("plugin"))
		{
		  sdf::ElementPtr pluginSDF = ii->GetSDF()->GetElement("plugin");
		  while (pluginSDF) 
		    {
		      if (pluginSDF->HasElement("ocean_effects"))
			{
			  sdf::ElementPtr oceanSDF = pluginSDF->GetElement("ocean_effects");
			  if ( oceanSDF->HasElement("currents") && oceanSDF->Get<bool>("currents"))
			    {

			      if ( this->ocean_effects.currents.model == ocean_currents_model_t::FVCOM)
				{

				  gzdbg << "Loading ocean model FVCOM for " << ii->GetName() << "..." << std::endl;
				  ocean_model_interfaces::ModelInterface* ocean_model = new ocean_model_interfaces::FVCOM(this->ocean_effects.currents.fvcom_uri);

				  this->models.pop_back();
				  std::tuple<gazebo::physics::ModelPtr,ocean_model_interfaces::ModelInterface*> replacement(ii,ocean_model);
				  this->models.push_back(replacement);
				  
				  gzdbg << "Loading ocean model FVCOM for " << ii->GetName() << "... Success." << std::endl;

				  // Epoch conversion: 1970-01-01 00:00:00 is Modified Julian Day 40587
				  double jtime = 40587.0*86400.0 + _info.simTime.Double();
				
				  // Initialize the cache.  Need to convert between SRSs.
				  this->ocean_effects.currents.fvcom_poCT
				    = OGRCreateCoordinateTransformation(&this->srs, &this->ocean_effects.currents.fvcom_srs );
				  ignition::math::Pose3d pose = ii->WorldPose();				
				  double sNorthing = pose.Pos().Y(); // native projection (source coordinates)
				  double sEasting = pose.Pos().X();
				  double tNorthing = sNorthing; // This is both the input and output to Transform() below.
				  double tEasting = sEasting;
				  if( this->ocean_effects.currents.fvcom_poCT == NULL
				      || !this->ocean_effects.currents.fvcom_poCT->Transform( 1, &tEasting, &tNorthing ) )
				    {
				      gzerr << "Projected coordinate transformation failed." << std::endl;
				    }
				  else
				    {

				      try
					{
					  ocean_model_interfaces::ModelData ocean_data = ocean_model->getData(
													      tEasting, // easting 
													      tNorthing, // northing
													      pose.Pos().Z(), // up
													      jtime); // time, modified Julian seconds
					  std::cout << "got currents east:" << ocean_data.u
						    << " north:" << ocean_data.v << std::endl;
					}
				      catch(const std::out_of_range& e)
					{
					  gzerr << "No currents data for starting position/time of model "
						<< ii->GetName() << " (easting:" << tEasting
						<< " northing:" << tNorthing
						<< " depth:" << pose.Pos().Z()
						<< " Julian date:" << jtime/86400.0
						<< " )" << std::endl; // probably too deep.
					}
				      
				    }
				}
			      else
				{
				  gzerr << "Unsupported ocean currents model type." << std::endl;
				  // @@@ this should not happen if we default to constant during the Load() phase.
				}
			    }
			}
		      pluginSDF = pluginSDF->GetNextElement();
		    }
		}
	    }
	}

      // @@@ presently deleting lrauv crashes gazebo.  But ceiling plane is ok to remove, so not sure why lrauv would be special other than that it is last in the list?  gazebo crashes even with this commented out.  I don't think it's the issue.
      auto ii = this->models.begin();
      while (ii != this->models.end())
	{
	  bool isgone = true;
	  for ( auto& iii : this->world->Models() )
	    {
	      if ( std::get<0>(*ii)->GetId() == iii->GetId() )
		{
		  isgone = false;
		}
	    }
	  if ( isgone ) // in the internal list but not in the list of active models
	    {
	      ii = this->models.erase(ii);
	    }
	  else
	    {
	      ++ii;
	    }
	}
      // @@@ modifying models from the list from this point on introduces a race condition doesn't it?  What if
      // model gets deleted between now and operating on it.  Depends on how plugins work.  Are their update
      // functions atomic?
      // @@@ we do this above so that we can maintain the ocean_model caches for each model.
      // @@@ the dynamics maybe could be simplified by some kind of dynamic creation of F=ma that wouldn't
      // @@@ require traversing the SDF for each model every update.  right - you load the parameters for the
      // @@@ hydro function.


      // For each model compute the hydrodynamic forces using the scheme
      // specified in the model's SDF.
      // Only a few hydrodynamic derivatives supported for now.
      // Traverse the model's SDF to find links with the <hydrodynamics> tag.
      // @@@ move this to when new models are found only.
      for ( auto& mm : this->models )
	{
	  gazebo::physics::ModelPtr ii = std::get<0>(mm);
	  if (ii->GetSDF()->HasElement("plugin"))
	    {
	      sdf::ElementPtr pluginSDF = ii->GetSDF()->GetElement("plugin");
	      while (pluginSDF)  // traverse plugins
		{

		  // Default currents.
		  double U = 0.0;
		  double V = 0.0;

		  if (pluginSDF->HasElement("ocean_effects"))
		    {
		      sdf::ElementPtr oceanSDF = pluginSDF->GetElement("ocean_effects");
		      if ( oceanSDF->HasElement("currents") && oceanSDF->Get<bool>("currents"))
			{
			
			  // Only FVCOM supported.
			  if ( this->ocean_effects.currents.model = ocean_currents_model_t::FVCOM )
			    {
			    
			      // Epoch conversion: 1970-01-01 00:00:00 is Modified Julian Day 40587
			      double jtime = 40587.0*86400.0 + _info.simTime.Double();
			    
			      // @@@ do conversions/srs
			      ignition::math::Pose3d pose = ii->WorldPose();				
			      double sNorthing = pose.Pos().Y(); // native projection (source coordinates)
			      double sEasting = pose.Pos().X();
			      double tNorthing = sNorthing; // This is both the input and output to Transform() below.
			      double tEasting = sEasting;
			      if( this->ocean_effects.currents.fvcom_poCT == NULL
				  || !this->ocean_effects.currents.fvcom_poCT->Transform( 1, &tEasting, &tNorthing ) )
				{
				  gzerr << "Projected coordinate transformation failed." << std::endl;
				}
			      else
				{
				  std::cout.precision(17);
				  //gzdbg << "easting" << tEasting << " northing:" << tNorthing << " jtime:" << std::fixed << jtime << std::endl;
				  ocean_model_interfaces::ModelInterface* ocean_model = std::get<1>(mm);
				  try
				    {
				      ocean_model_interfaces::ModelData ocean_data = ocean_model->getData(
													  tEasting, // easting 
													  tNorthing, // northing
													  pose.Pos().Z(), // up
													  jtime); // time, modified Julian seconds
				      U = ocean_data.v; // reversed convention
				      V = ocean_data.u;
				      //gzdbg << "Currents for " << ii->GetName() << " at t:" << jtime << " n:" << tNorthing << " e:" << tEasting << " d:" << pose.Pos().Z() << " U:" << U << " V:" << V << std::endl;
				    
				      geometry_msgs::Vector3 msg;
				      msg.x = U; // @@@ need our own message type to make the fields make sense.
				      msg.y = V;
				      msg.z = ocean_data.w;
				      this->pub_.publish(msg); // needs a separate one for each model @@@
				    
				    }
				  catch(const std::out_of_range& e)
				    {
				      gzwarn << "currents out of range" << std::endl; // probably too deep.
				      U = 0.0;
				      V = 0.0;
				    }
				
				}
			    }
			}
		    }

		
		  if (pluginSDF->HasElement("hydrodynamics"))
		    {
		      sdf::ElementPtr hydroSDF = pluginSDF->GetElement("hydrodynamics");


		      // @@@ need to buffer currents and add an option to reduce update rate.
		    

		      // All models need certain physical constants
		      double g = ii->GetWorld()->Gravity().Z();

		      // @@@@ need to move this into the model initialzation bit.
		      // @@@ also need to try multiple LRAUV.
		      while (hydroSDF)  // traverse <hydrodynamics> tags.
			{
			
			  // Apply a hydrodynamics model according to the type specified
			  if (!hydroSDF->HasAttribute("method") )
			    {
			      gzerr << "<hydrodynamics> tag must have the method attribute, i.e. method=" << std::endl;
			    }
			  else
			    {
			      if (hydroSDF->Get<std::string>("method") == "body_derivatives")
				{
				  gazebo::physics::LinkPtr body_link = ii->GetLink(hydroSDF->Get<std::string>("reference"));
				
				  // @@@@ move this into a separate function of course.  and make robust wrt missing derivatives.

				  double m = body_link->GetInertial()->Mass();
				
				  double Xuu = hydroSDF->Get<double>("Xuu");
				  double Yv = hydroSDF->Get<double>("Yv");
				  double Zw = hydroSDF->Get<double>("Zw");
				  double Kp = hydroSDF->Get<double>("Kp");
				  double Mq = hydroSDF->Get<double>("Mq");
				  double Nr = hydroSDF->Get<double>("Nr");
				  double zGB = hydroSDF->Get<double>("zGB");
				
				  // Gazebo body-frame velocities and rates: Fwd-Port-Up
				  ignition::math::Vector3d vel_lin = body_link->RelativeLinearVel();
				  ignition::math::Vector3d vel_ang = body_link->RelativeAngularVel();

				  // Convert to SNAME notation
				  double u = vel_lin[0];
				  double v = -vel_lin[1];
				  double w = -vel_lin[2];
				  double p = vel_ang[0];
				  double q = -vel_ang[1];
				  double r = -vel_ang[2];
				
				  // Gazebo pose
				  ignition::math::Pose3d pose = body_link->WorldPose();
				
				  // Convert to SNAME notation
				  // @@@ roll, pitch yaw are a different convention ("fixed axis"); not euler angles
				  double phi = pose.Rot().Roll();
				  double theta = -pose.Rot().Pitch();
				  double psi = M_PI/2.0-pose.Rot().Yaw();
				
				  // compute relative currents
				  double ur = u - (cos(psi)*U + sin(psi)*V);  // @@@ 2D only.
				  double vr = v - (-sin(psi)*U + cos(psi)*V);
				  double wr = w;
				
				  // model here.
				  double X = Xuu*ur*fabs(ur);
				  double Y = Yv*vr;
				  double Z = Zw*wr;
				  double K = Kp*p + zGB*sin(phi)*m*g;
				  double M = Mq*q + zGB*sin(theta)*m*g;
				  double N = Nr*r;
				
				  // Apply forces to body link.
				  ignition::math::Vector3d force = {X,-Y,-Z};
				  ignition::math::Vector3d torque = {K,-M,-N};
				  body_link->AddRelativeForce(force);
				  body_link->AddRelativeTorque(torque);
			    
				}
			      else if (hydroSDF->Get<std::string>("method") == "fin_to_body_derivatives")
				{
				  gazebo::physics::LinkPtr body_link = ii->GetLink(hydroSDF->Get<std::string>("reference_body"));
				  gazebo::physics::JointPtr joint = ii->GetJoint(hydroSDF->Get<std::string>("reference_joint"));
				
				  double Kuudf = hydroSDF->Get<double>("Kuudf");
				  double Muudf = hydroSDF->Get<double>("Muudf");
				  double Nuudf = hydroSDF->Get<double>("Nuudf");
				  //double Nuur = hydroSDF->Get<double>("Nuur"); // this may belong more in the body hydro.
				
				  // Gazebo body-frame velocities and rates: Fwd-Port-Up
				  ignition::math::Vector3d vel_lin = body_link->RelativeLinearVel();
				  ignition::math::Vector3d vel_ang = body_link->RelativeAngularVel();
				
				  // Convert to SNAME notation
				  double u = vel_lin[0];
				  double v = -vel_lin[1];
				  double w = -vel_lin[2];
				  double p = vel_ang[0];
				  double q = -vel_ang[1];
				  double r = -vel_ang[2];
				
				  // Gazebo pose
				  ignition::math::Pose3d pose = body_link->WorldPose();
				
				  // Convert to SNAME notation
				  // @@@ roll, pitch yaw are a different convention ("fixed axis"); not euler angles
				  double phi = pose.Rot().Roll();
				  double theta = -pose.Rot().Pitch();
				  double psi = M_PI/2.0-pose.Rot().Yaw();
				
				  // compute relative currents
				  double ur = u - (cos(psi)*U + sin(psi)*V);  // @@@ 2D only.
				  double vr = v - (-sin(psi)*U + cos(psi)*V);
				  double wr = w;
				
				  // get fin angle
				  double fin_angle = joint->Position(0);
				
				  // Compute body forces.
				  double K = Kuudf*ur*fabs(ur)*fin_angle; // small angles
				  double M = Muudf*ur*fabs(ur)*fin_angle;
				  double N = Nuudf*ur*fabs(ur)*fin_angle;
				
				  // Apply forces to body.
				  ignition::math::Vector3d torque = {K,-M,-N};
				  body_link->AddRelativeTorque(torque);
				
				}
			      else if (hydroSDF->Get<std::string>("method") == "propeller_to_body_derivatives")
				{
				  gazebo::physics::LinkPtr body_link = ii->GetLink(hydroSDF->Get<std::string>("reference_body"));
				  gazebo::physics::JointPtr shaft = ii->GetJoint(hydroSDF->Get<std::string>("reference_joint"));


				
				  double Xrpm = hydroSDF->Get<double>("Xrpm");
				  double shaft_speed_rpm = shaft->GetVelocity(0)/M_PI/2.0*60.0*PROP_RPM_FACTOR;

				  // Compute forces to apply to main body.
				  // Given terminal velocity versus speed, and a 2nd order Taylor series
				  // expansion for propeller thrust as a function of n and u,
				  //   thrust = k0*n^2 + k1*n*u + k2*u^2
				  // and assuming quadratic
				  // drag,
				  //   u(inf) = Xuu*thrust(inf)  
				  // the terminal velocity u is linearly dependent on n.
				  //   u(inf) = k*n
				  // It is impossble
				  // to independently determine the three parameters of the Taylor series and
				  // the drag coefficient from such data. (There is only one unknown coefficient,
				  // the slope of the line for n vs u.) 
				  // However, given an independent estimate of drag coefficient, a
				  // consistent set of coefficients can be chosen.  The simplest is to assume
				  // only the bollard term is non-zero.  A model for propeller thrust is then
				  // X = k * n^2
				  // This will yield the desired steady-state response but is a crude model for
				  // propeller thrust (it accurately predicts neither bollard nor the reduction of
				  // thrust with speed for constant rpm).
				  // However, this choice is also results in non-zero thrust at zero speed, i.e. it
				  // is the only choice for which two of three coefficients can be set to zero.
				  // A more realistic two-parameter model could be specified if bollard data were
				  // available.
				  double X = Xrpm*shaft_speed_rpm*fabs(shaft_speed_rpm);
				  double Y = 0.0;
				  double Z = 0.0;

				  // Apply forces to body.
				  ignition::math::Vector3d force = {X,-Y,-Z};
				  body_link->AddRelativeForce(force);

				  //if (simulate_battery)
				  if (initializing)
				    {
				      lastSimTime = _info.simTime;
				      initializing = false;
				    }
				  else if (true)
				    {
				      counter++;
				      double timestep = 0.01; // In seconds. Need to find a way to get this from Gazebo physics engine instead as it could be variable

				      // NEW TIMESTAMP HANDLING FROM THE GAZEBO ENGINE
				      gazebo::common::Time gz_dt = _info.simTime - lastSimTime;
				      lastSimTime = _info.simTime;
				      timestep = gz_dt.Double();
				      // END NEW TIMESTAMP HANDLING

				      double absolute_rpm = fabs(shaft_speed_rpm); // To be fully correct we should differentiate fro fwd and reverse to account for
				                                                   // different propeller efficiency in reverse, but we do not have data for reverse (or braking)

				      // Quadratic model coefficients from spreadsheet polynomial fit, forcing fit crossing by the hotel load
				      double E2 = 0.00073918403327;
				      double E1 = -0.1543691315368;
				      double E0 = 32.0; // Hotel load

				      double whr_used_in_timestep = (E2 * absolute_rpm * absolute_rpm + E1 * absolute_rpm + E0) * (timestep / 3600.0);

				      battery_whrs -= whr_used_in_timestep;

				      std_msgs::Float64 msg;
				      msg.data = battery_whrs;
				      this->battery_pub_.publish(msg);

				      msg.data = absolute_rpm;
				      this->rpm_pub_.publish(msg);

				      msg.data = timestep;
				      this->dt_pub_.publish(msg);

                                      // Get the system time, not rostime, otherwise cloudwatch complains of skew
                                      boost::posix_time::ptime btime = boost::posix_time::microsec_clock::universal_time();
				      // Metrics for Cloudwatch
				      ros::Time now = ros::Time::now();
				      ros_monitoring_msgs::MetricList metric_list;
				      metric_list.metrics.resize(1);
				      metric_list.metrics[0].header.seq = counter;
				      //metric_list.metrics[0].header.stamp = now;
				      metric_list.metrics[0].header.stamp = ros::Time::fromBoost(btime);
				      metric_list.metrics[0].header.frame_id = "AUV";
				      metric_list.metrics[0].metric_name = "battery";
				      metric_list.metrics[0].unit = ros_monitoring_msgs::MetricData::UNIT_COUNT;
				      metric_list.metrics[0].value = battery_whrs;
				      //metric_list.metrics[0].time_stamp = now;
				      metric_list.metrics[0].time_stamp = ros::Time::fromBoost(btime);
				      this->metrics_pub_.publish(metric_list);
				    }
				
				}
			      else
				{
				  gzerr << "<hydrodynamics> tag for " << ii->GetName() << "specifies an unsupported method." << std::endl;
				}
			    }
			  hydroSDF = hydroSDF->GetNextElement("hydrodynamics");
			}
		    }
		  pluginSDF = pluginSDF->GetNextElement("plugin");
		}
	    }
	}
    }

  
  private: gazebo::physics::WorldPtr world;
  private: sdf::ElementPtr sdf;
  private: OGRSpatialReference srs; // spatial reference system

  private: ocean_effects_t ocean_effects;
  
  protected: gazebo::event::ConnectionPtr updateConnection;
  protected: gazebo::common::Time lastSimTime;

    // @@@ need to templatize this or something to allow for mulple model types.
    // @@@ will some models have hydrodynamics but not currents?  perhaps, but storing a pointer is no big deal.
  private: std::list< std::tuple<gazebo::physics::ModelPtr,ocean_model_interfaces::ModelInterface*> > models;


  private: ros::NodeHandle* rosnode_;
  private: ros::Publisher pub_;

  private: ros::Publisher battery_pub_;
  private: ros::Publisher rpm_pub_;
  private: ros::Publisher dt_pub_;
  private: double battery_whrs;
  private: bool simulate_battery;
  private: bool initializing; // To initialize the sim time

  private: ros::Publisher metrics_pub_;
  int counter;

  };

  // Register this plugin with the simulator
  GZ_REGISTER_WORLD_PLUGIN(WindHydroPlugin)
}
