/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream> /* debug */

#include <cassert>
#include <cmath>
#include <vector>

#include "MagDataReader.h"
#include "Coordinates.h"


/*
    Binary search across one dimension of a netCDF variable. Based on
    bisect_left() from Python's bisect module.

    Returns the index i at which x could be inserted to maintain sorted order.

    The return value i is such that all e in a[:i] have e < x, and all e in
    a[i:] have e >= x. So if x already appears in the list, a.insert(i, x) will
    insert just before the leftmost x already there.
 */
template <typename T>
static
std::size_t bisect_left(const netCDF::NcVar& var, T x, std::size_t dim = 0)
{
    std::vector<std::size_t> index(var.getDimCount(), 0);
    std::size_t lo = 0, hi = var.getDim(dim).getSize();

    while (lo < hi) {
        std::size_t mid = index[dim] = (lo + hi) / 2;

        T value;
        std::cout << "getting index " << index[0] << ", " << index[1] << " at " << __LINE__ << std::endl;
        var.getVar(index, &value);

        if (value < x)
            lo = mid + 1;
        else
            hi = mid;
    }

    return lo;
}


/*
    Finds the index i of the value closest to x.
*/
template <typename T>
static
std::size_t nearest_neighbor(const netCDF::NcVar& var, T x, std::size_t dim = 0)
{
    bool has_left = false, has_right = false;
    T left, right;

    std::vector<std::size_t> index(var.getDimCount(), 0);
    std::size_t i = index[dim] = bisect_left(var, x, dim);

    if (i < var.getDim(dim).getSize()) {
        has_right = true;
        std::cout << "getting index " << index[0] << ", " << index[1] << " at " << __LINE__ << std::endl;
        var.getVar(index, &right);
    }
    
    std::cout << "i is " << i << " index[dim] is " << index[dim] << std::endl;
    if (i >= 1) {
        has_left = true;
        index[dim] -= 1;
        std::cout << "getting index " << index[0] << ", " << index[1] << " at " << __LINE__ << std::endl;
        var.getVar(index, &left);
    }

    if (!has_left && !has_right)
        return -1;  // array is empty
    else if (has_left && !has_right)
        return i - 1;
    else if (has_right && !has_left)
        return i;
    else if (std::abs(left - x) < std::abs(right - x))
        return i - 1;
    else
        return i;
}


static
UTMZone zoneFromMagDataFile(const netCDF::NcFile& dataFile)
{
    UTMZone zone;
    dataFile.getAtt("utm_zone_x").getValues(&zone.x);
    dataFile.getAtt("utm_zone_y").getValues(&zone.y);
    return zone;
}


MagDataReader::MagDataReader(const std::string& path)
    : dataFile(path, netCDF::NcFile::read),
      zone(zoneFromMagDataFile(dataFile))
{
    this->northing = this->dataFile.getVar("northing");
    this->easting = this->dataFile.getVar("easting");
    this->Btotal = this->dataFile.getVar("Btotal");

    assert(this->Btotal.getDim(0).getName() == "northing");
    assert(this->Btotal.getDim(1).getName() == "easting");
}


double MagDataReader::valueAt(const CoordUTM& loc) const
{
    // We can only return values within our 
    if (loc.zone != this->zone) {
        return nan("UTM zone mismatch");
    }

    // Pick the closest data point to this location
    std::vector<std::size_t> index(this->Btotal.getDimCount(), 0);
    index[0] = nearest_neighbor(this->northing, loc.northing);
    index[1] = nearest_neighbor(this->easting, loc.easting);

    std::cout << "getting index " << index[0] << ", " << index[1] << " at " << __LINE__ << std::endl;

    double value;
    this->Btotal.getVar(index, &value);
    return value;
}
