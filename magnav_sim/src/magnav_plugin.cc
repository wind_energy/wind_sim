/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/


#include <cstdlib> /* temporary */
#include <functional>
#include <iomanip> /* temporary */
#include <limits>
#include <unistd.h> /* temporary */

#include <boost/pointer_cast.hpp>

#include <gazebo/physics/physics.hh>

#include <ignition/math/Pose3.hh>

#include <sensor_msgs/MagneticField.h>

#include "magnav_plugin.hh"


using namespace gazebo;


GZ_REGISTER_SENSOR_PLUGIN(MagNavSensor);


MagNavSensor::MagNavSensor() : SensorPlugin()
{
    reader = NULL;
    transformer = NULL;
    utm_transformer = NULL;
}


MagNavSensor::~MagNavSensor()
{
    if (this->world_update_conn.get()) {
        this->world_update_conn.reset();
    }
    this->node->shutdown();
}


void MagNavSensor::Load(sensors::SensorPtr sensor_, sdf::ElementPtr sdf)
{
    gzmsg << "I am running in PID " << getpid() << std::endl;

    // Get the Sensor instance that we are attached to
    this->sensor =
        std::dynamic_pointer_cast<gazebo::sensors::MagnetometerSensor>(sensor_);
    
    if (this->sensor == NULL) {
        ROS_FATAL("Error! Unable to convert sensor pointer!");
        return;
    }

    
    // Get the link we are attached to.
    // Note: We have to use boost::dynamic_pointer_cast here because these are
    // not using C++11 std::shared_ptr<>s.
    auto world = gazebo::physics::get_world(this->sensor->WorldName());
    this->parent_link = boost::dynamic_pointer_cast<gazebo::physics::Link>(
        world->EntityByName(this->sensor->ParentName()));


    // Create a transform from simulation coordinates to lat/lon
    this->srs.importFromEPSG(sdf->GetElement("projection")->Get<int>("epsg"));
    this->tsrs.importFromEPSG(4326); // lat/lon
    this->transformer = OGRCreateCoordinateTransformation(&this->srs, &tsrs);

    // Initialize the MagDataReader
    this->reader =
        std::make_unique<MagDataReader>(sdf->Get<std::string>("dataFile"));
    this->utm_transformer =
        std::make_unique<UTMLatLonTransformer>(this->reader->zone);


    // Initialize the MagneticField message we will be publishing
    this->msg.header.frame_id = sdf->Get<std::string>("frameName");
    bzero(&this->msg.magnetic_field_covariance,
        sizeof(this->msg.magnetic_field_covariance));


    // Initialize the ROS node
    if (!ros::isInitialized()) {
        ROS_FATAL("ROS has not been initialized properly...");
        return;
    }

    this->node = new ros::NodeHandle(
        sdf->Get<std::string>("robotNamespace") + "/");
    this->publisher = node->advertise<sensor_msgs::MagneticField>(
        sdf->Get<std::string>("topicName", "/mag").first, 1);


    // Subscribe to Gazebo world update events
    this->world_update_conn = gazebo::event::Events::ConnectWorldUpdateBegin(
        std::bind(&MagNavSensor::OnWorldUpdate, this, std::placeholders::_1));
}


CoordLatLon MagNavSensor::GetPosition() const
{
    ignition::math::Pose3d pose =
        this->sensor->Pose() + this->parent_link->WorldPose();

    double x[] = { pose.Pos().X() };
    double y[] = { pose.Pos().Y() };
    double z[] = { 0 };
    this->transformer->Transform(sizeof(x) / sizeof(x[0]), x, y, z );
    return { .latitude = y[0], .longitude = x[0] };
}


void MagNavSensor::OnWorldUpdate(const gazebo::common::UpdateInfo &info)
{
    // Throttle our updates to the desired rate
    bool isTimeToUpdate =
        (this->sensor->UpdateRate() < std::numeric_limits<double>::epsilon()) ||
        ((info.simTime - this->last_update_time).Double()
            >= 1.0/this->sensor->UpdateRate());
    
    if (!isTimeToUpdate)
        return;

    this->last_update_time = info.simTime;

    // Nobody cares
    if (0 && this->publisher.getNumSubscribers() == 0)
        return;

    // Query the magnetism data at our current position
    CoordLatLon position = this->GetPosition();
    CoordUTM utm_coord = this->utm_transformer->transform(position);
    double value = this->reader->valueAt(utm_coord);

    // Update and publish the magnetic field message
    this->msg.header.stamp.sec = info.simTime.sec;
    this->msg.header.stamp.nsec = info.simTime.nsec;
    this->msg.header.seq ++;
    this->msg.magnetic_field.z = value;
    this->publisher.publish(this->msg);

    // Spin once so that ROS can send the message out to subscribers
    ros::spinOnce();
}
