/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <gdal/ogr_spatialref.h>

#include "UTMLatLonTransformer.h"
#include "Coordinates.h"

#include <iostream> // DEBUG


UTMLatLonTransformer::UTMLatLonTransformer(const UTMZone zone)
{
    this->utm_zone = zone;

    // Adapted from https://stackoverflow.com/a/10239676
    this->utm_coordinate_system.SetWellKnownGeogCS("WGS84");
    this->utm_coordinate_system.SetUTM(zone.x, zone.is_northern() ? 1 : -1);

    this->wgs84_coordinate_system = utm_coordinate_system.CloneGeogCS();

    this->utm_to_wgs84 = OGRCreateCoordinateTransformation(
        /* from */ &utm_coordinate_system,
        /* to */ wgs84_coordinate_system
    );

#if 0
    // This API might not be available?
    this->wgs84_to_utm = this->utm_to_wgs84->GetInverse();
#else
    this->wgs84_to_utm = OGRCreateCoordinateTransformation(
        /* from */ wgs84_coordinate_system,
        /* to */ &utm_coordinate_system
    );
#endif
}


UTMLatLonTransformer::~UTMLatLonTransformer()
{
    delete this->wgs84_to_utm;
    delete this->utm_to_wgs84;
    delete this->wgs84_coordinate_system;
}

CoordLatLon UTMLatLonTransformer::transform(const CoordUTM& in) const
{
    double x[] = { in.easting };
    double y[] = { in.northing };
    double z[] = { 0 };
    this->utm_to_wgs84->Transform(sizeof(x) / sizeof(x[0]), x, y, z );
    return { .latitude = y[0], .longitude = x[0] };
}

CoordUTM UTMLatLonTransformer::transform(const CoordLatLon& in) const
{
    double x[] = { in.longitude };
    double y[] = { in.latitude };
    double z[] = { 0 };
    this->wgs84_to_utm->Transform(sizeof(x) / sizeof(x[0]), x, y, z );
    return { .northing = y[0], .easting = x[0], .zone = this->utm_zone };
}