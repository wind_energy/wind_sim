/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __MAGNAV_PLUGIN_H__
#define __MAGNAV_PLUGIN_H__


#include <memory>
#include <string>

#include <gazebo/physics/Link.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/UpdateInfo.hh>
#include <gazebo/sensors/MagnetometerSensor.hh>

#include <gdal/ogr_spatialref.h>

#include <ros/ros.h>

#include "Coordinates.h"
#include "MagDataReader.h"
#include "UTMLatLonTransformer.h"


namespace gazebo {

class MagNavSensor : public SensorPlugin
{
public:

    /// \brief Constructor
    MagNavSensor();

    /// \brief Destructor
    virtual ~MagNavSensor();

    /// \brief Load the sensor.
    /// \param sensor_ pointer to the sensor.
    /// \param sdf_ pointer to the sdf config file.
    virtual void Load(sensors::SensorPtr sensor_, sdf::ElementPtr sdf_);

protected:
    /// \brief Update the sensor
    void OnWorldUpdate(const gazebo::common::UpdateInfo &info);

private:
    /// \brief Get the latitude and longitude of the sensor
    CoordLatLon GetPosition() const;

    /// \brief ROS node handle
    ros::NodeHandle* node;

    /// \brief  Data publisher
    ros::Publisher publisher;

    /// \brief Pointer to the update event connection.
    gazebo::event::ConnectionPtr world_update_conn;

    /// \brief Pointer to the sensor.
    std::shared_ptr<gazebo::sensors::MagnetometerSensor> sensor;

    common::Time last_update_time;

    gazebo::physics::LinkPtr parent_link;

    // Note:
    // This is modified in place and therefore cannot be concurrently accessed.
    sensor_msgs::MagneticField msg;


    OGRSpatialReference srs, tsrs;
    OGRCoordinateTransformation* transformer;

    std::unique_ptr<MagDataReader> reader;
    std::unique_ptr<UTMLatLonTransformer> utm_transformer;
};

}; // namespace gazebo


#endif
