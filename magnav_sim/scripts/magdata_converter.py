#!/usr/bin/env python3
import argparse

import scipy.io


parser = argparse.ArgumentParser()
parser.add_argument('input')
parser.add_argument('output')
args = parser.parse_args()


mat = scipy.io.loadmat(args.input)

utm_zone_x, _, utm_zone_y = mat['parms'][0,0]['utmzone'][0].partition(' ')
utm_zone = (int(utm_zone_x), utm_zone_y)

northing = mat['magMap'][0,0]['Northing'][0]
easting = mat['magMap'][0,0]['Easting'][0]
btotal = mat['magMap'][0,0]['Btotal']


with scipy.io.netcdf.netcdf_file(args.output, 'w') as nc:
    nc.createDimension('northing', northing.shape[0])
    nvar = nc.createVariable('northing', 'double', ('northing',))
    nvar[:] = northing

    nc.createDimension('easting', easting.shape[0])
    evar = nc.createVariable('easting', 'double', ('easting',))
    evar[:] = easting

    # Confirmed dimension ordering with Greg Schultz 2021-12-17
    bvar = nc.createVariable('Btotal', 'double', ('northing', 'easting'))
    bvar[:] = btotal

    nc.utm_zone_x, nc.utm_zone_y = utm_zone
