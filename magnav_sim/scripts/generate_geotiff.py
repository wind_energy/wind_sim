#!/usr/bin/env python3
import argparse

import numpy as np
import rasterio
import scipy.io

# Rasterio warns against using Python GDAL bindings at the same time, but it
# specifically refers to osgeo.gdal -- are we safe here?
#
# See: https://rasterio.readthedocs.io/en/latest/topics/switch.html
from osgeo import osr


parser = argparse.ArgumentParser()
parser.add_argument('input')
parser.add_argument('output')
args = parser.parse_args()


# Colormap adapted from matplotlib's _cm.py
jet = {
    'r': ((0, 0, 0), (0.35, 0, 0), (0.66, 1, 1), (0.89, 1, 1), (1, 0.5, 0.5)),
    'g': ((0, 0, 0), (0.125, 0, 0), (0.375, 1, 1), (0.640, 1, 1), (0.910, 0, 0),
          (1, 0, 0)),
    'b': ((0, 0.5, 0.5), (0.11, 1, 1), (0.34, 1, 1), (0.65, 0, 0), (1, 0, 0)),
}

def cmap(x, map=jet):
    lerp = lambda a, b, t: a + (b - a) * t

    # Inner function does it for one channel, outer for all channels
    def _cmap(x, map):
        for i in range(len(map)):
            if map[i][0] < x:
                continue

            # Linearly interpolate from previous color value to this one
            t = (x - map[i-1][0]) / (map[i][0] - map[i-1][0])
            return lerp(map[i-1][2], map[i][1], t)
    
    return tuple((_cmap(x, map[c]) for c in 'rgb'))


# Load data from the NetCDF file
with scipy.io.netcdf.netcdf_file(args.input, 'r', mmap=False) as nc:
    northing = nc.variables['northing'].data
    easting = nc.variables['easting'].data
    btotal = nc.variables['Btotal'].data
    utm_zone = (int(nc.utm_zone_x), nc.utm_zone_y)


# Set up our UTM coordinate reference system.
# Adapted from https://stackoverflow.com/a/10239676
crs = osr.SpatialReference()
crs.SetWellKnownGeogCS('WGS84')
crs.SetUTM(utm_zone[0], utm_zone[1] >= b'N')

# Create ground control points from each corner
gcps = []
for i in (0, -1):
    for j in (0, -1):
        gcps.append(rasterio.control.GroundControlPoint(
            row=(i % northing.shape[0]),
            col=(j % easting.shape[0]),
            x=easting[j],
            y=northing[i],
        ))

# Create an affine transform from the ground control points
transform = rasterio.transform.from_gcps(gcps)

# Write out the GeoTIFF
opts = {
    'driver': 'GTiff',
    'height': btotal.shape[0],
    'width': btotal.shape[1],
    'count': 1,  # number of bands
    'dtype': 'int8',
    'crs': crs.ExportToWkt(),
    'transform': transform,
}
with rasterio.open(args.output, 'w', **opts) as gt:
    # Scale btotal to 0..255
    btotal = 255 * (btotal - np.min(btotal)) / (np.max(btotal) - np.min(btotal))
    btotal = btotal.astype(int)
    
    # Write it out
    gt.write(btotal, 1)

    # Generate a color map
    gt.write_colormap(1,
        { k: tuple(int(255*x) for x in cmap(k/255.0)) + (255,)
          for k in range(256) }
    )
